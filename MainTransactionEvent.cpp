/*
    I N C L U D E S
*/
#include <stdio.h>
#include <stdlib.h>

#include "MainTransactionEvent.hpp"
#include "trcerr_e.h"


/*
    S T A T I C   G L O B A L   V A R I A B L E S
*/
static CHAR szModG[] = "$MOD$ 141024 1000 MainTransactionEvent.DLL";
static CHAR szSegG[] = "$SEG$ 141024 1000 MainTransactionEvent.CPP";


/*
    I M P L E M E N T A T I O N
    ===========================

    class MainTransactionEvent
*/
MainTransactionEvent *MainTransactionEvent::pThisM = 0;


MainTransactionEvent::MainTransactionEvent()
{

    TrcErrInit(TRC_MODID_MainTransactionEvent, szModG);
    TrcWritef(TRC_FUNC, "> MainTransactionEvent::MainTransactionEvent");

    FrmSetName(CCTAFW);
	FrmRegisterForEvents(CC_JRN_FW);
    pThisM = this;

    TrcWritef(TRC_FUNC, "< MainTransactionEvent::MainTransactionEvent");
}


MainTransactionEvent &MainTransactionEvent::Current()
{
    return (*pThisM);
}


SHORT MainTransactionEvent::AddStep(
        Step *pcoStep)
{
    SHORT sRc = CCFRMW_RC_OK;

    TrcWritef(TRC_FUNC, "> MainTransactionEvent::AddStep(%s)", pcoStep->FuncName());

    coaStepsM.Add(pcoStep);

    TrcWritef(TRC_FUNC, "< MainTransactionEvent::AddStep -> %d", sRc);
    return (sRc);
}


SHORT MainTransactionEvent::OnFrmEvent(
        PCHAR szSender,
        SHORT sEventId,
        VOID *pData,
        SHORT sDataLen)
{
    SHORT sRc;

	char  chData[1024];
	CCString  strData;

    TrcWritef(TRC_INFO, "> MainTransactionEvent::OnFrmEvent %d from %s",
                                                        sEventId, szSender);
	
	if (strcmp(szSender,CC_JRN_FW)==0)
	{
		TrcWritef(TRC_INFO, "> I got the event sEventId = %d" ,sEventId); 
		
		if (sEventId == 1)
		{
			TrcWritef(TRC_INFO, "> Journal Data = %s" ,(PCHAR)pData); 

			sprintf(chData,"%s",(CHAR*)pData);

			strData = chData;

			if(strData.includes("DEP Error:"))
			{
				//sRc = EJErrorMap();
			}
				
		}
	}
	
	/*
        Route the event to the base class
    */
    sRc = OnFrmEventBaseClass(szSender, sEventId, pData, sDataLen);

    TrcWritef(TRC_INFO, "< MainTransactionEvent::OnFrmEvent");
    return(CCFRMW_RC_OK);  

}


SHORT MainTransactionEvent::OnFrmRequest(
        SHORT sMethodId,
        VOID *pData1,
        SHORT sDataLen1,
        VOID *pData2,
        SHORT sDataLen2,
        VOID *pData3,
        SHORT sDataLen3,
        ULONG ulTimeOut)
{
    SHORT sRc;

    TrcWritef(TRC_INFO, "> MainTransactionEvent::OnFrmRequest %d", sMethodId);

    switch(sMethodId)
    {
		case CCTAFW_FUNC_INIT_RESOURCES:
        
			sRc = OnFrmRequestBaseClass(sMethodId, pData1, sDataLen1,
                            pData2, sDataLen2, pData3, sDataLen3, ulTimeOut);
            break;

        case CCTAFW_FUNC_PROCESS_STEP:
            sRc = ProcessStep((PCHAR)pData1, (PCHAR)pData2, (PCHAR)pData3);
            break;

        default:
            sRc = OnFrmRequestBaseClass(sMethodId, pData1, sDataLen1,
                            pData2, sDataLen2, pData3, sDataLen3, ulTimeOut);
            break;

    }

    TrcWritef(TRC_INFO, "< MainTransactionEvent::OnFrmRequest -> %d", sRc);
    return(sRc);
}



SHORT MainTransactionEvent::ProcessStep(
        PCHAR pchSectNextStep,
        PCHAR szStepFunc,
        PCHAR szStepParams)
{
    int iIdx = 0;
    int iNumSteps = coaStepsM.GetSize();
    BOOL fFound = FALSE;
    unsigned int uiStepLen = strlen(szStepFunc);
    Step *pcoStep;
    SHORT sRc;

    TrcWritef(TRC_FUNC, "> MainTransactionEvent::ProcessStep(%s, %s, %s)",
                                pchSectNextStep, szStepFunc, szStepParams);

    while (!fFound && (iIdx < iNumSteps))
    {
        pcoStep = (Step *)coaStepsM.GetAt(iIdx);
        if ((uiStepLen == strlen(pcoStep->FuncName())) &&
            (strcmp(szStepFunc, pcoStep->FuncName()) == 0))
        {
            fFound = TRUE;

            TrcWritef(TRC_INFO, "Found index %d, calling Process", iIdx);

            sRc = pcoStep->Process(pchSectNextStep, szStepParams);
        }
        else
        {
            iIdx++;
        }
    }

    if (!fFound)
    {
        TrcWritef(TRC_INFO, "Not found, calling base class");

        sRc = OnFrmRequestBaseClass(CCTAFW_FUNC_PROCESS_STEP,
                                pchSectNextStep, CCTAFW_SECT_NEXT_MAX_LEN,
                                szStepFunc, strlen(szStepFunc) + 1,
                                szStepParams, strlen(szStepParams) + 1, 0);
    }
/* KARLI start 970602 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
    /*
        Now this is very specific:
        If the next state is state 000 or no next state given we go to the
        TRAN_END step(default close) to completely finish the transaction.
    */
    if (sRc == CCTAFW_RC_JUMP_LABEL)
    {
        if (!*pchSectNextStep || !strcmp(pchSectNextStep, "000"))
        {
            strcpy(pchSectNextStep, "TRAN_END");    // For now !!!!!
        }
    }
/* KARLI end   970602 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

    TrcWritef(TRC_FUNC, "< MainTransactionEvent::ProcessStep -> %d, next %s",
                                                    sRc, pchSectNextStep);
    return(sRc);
}


/*
    I M P L E M E N T A T I O N
    ===========================

    class Step
*/
//SHORT Step::FrmSendEvent(
//        SHORT sEventId,
//        VOID *pData,
//        SHORT sDataLen)
//{
//    return (MainTransactionEvent::Current().FrmSendEvent(sEventId,
//                                                            pData, sDataLen));
//}
//
//
//SHORT Step::FrmSendEventSync(
//        SHORT sEventId,
//        VOID *pData,
//        SHORT sDataLen)
//{
//    return (MainTransactionEvent::Current().FrmSendEventSync(sEventId,
//                                                            pData, sDataLen));
//}
//
//
//SHORT Step::FrmEnterCritSec()
//{
//    return (MainTransactionEvent::Current().FrmEnterCritSec());
//}
//
//
//SHORT Step::FrmExitCritSec()
//{
//    return (MainTransactionEvent::Current().FrmExitCritSec());
//}
//
//
//SHORT Step::ProcessStep(
//        PCHAR pchSectNextStep,
//        PCHAR szStepFunc,
//        PCHAR szStepParams)
//{
//    return (MainTransactionEvent::Current().FrmResolve(CCTAFW,
//                                CCTAFW_FUNC_PROCESS_STEP,
//                                pchSectNextStep, CCTAFW_SECT_NEXT_MAX_LEN,
//                                szStepFunc, strlen(szStepFunc) + 1,
//                                szStepParams, strlen(szStepParams) + 1));
//}
//
//
//SHORT Step::ProcessBaseStep(
//        PCHAR pchSectNextStep,
//        PCHAR szStepFunc,
//        PCHAR szStepParams)
//{
//    return (MainTransactionEvent::Current().OnFrmRequestBaseClass(
//                                CCTAFW_FUNC_PROCESS_STEP,
//                                pchSectNextStep, CCTAFW_SECT_NEXT_MAX_LEN,
//                                szStepFunc, strlen(szStepFunc) + 1,
//                                szStepParams, strlen(szStepParams) + 1, 0));
//
//}
//
//
//SHORT Step::ProcessStepAsync(
//        CCFRMW_JOB_STATUS *pJobState,
//        PCHAR pchSectNextStep,
//        PCHAR szStepFunc,
//        PCHAR szStepParams)
//{
//    return (MainTransactionEvent::Current().FrmAsyncResolve(CCTAFW,
//                                CCTAFW_FUNC_PROCESS_STEP, pJobState,
//                                pchSectNextStep, CCTAFW_SECT_NEXT_MAX_LEN,
//                                szStepFunc, strlen(szStepFunc) + 1,
//                                szStepParams, strlen(szStepParams) + 1));
//}
//
//
//
//
//BOOL Step::FrmCheckAsyncCompletion(
//        CCFRMW_JOB_STATUS *pJobState,
//        ULONG ulTimeOut)
//{
//    return (MainTransactionEvent::Current().FrmCheckAsyncCompletion(
//                                                    pJobState, ulTimeOut));
//}

/*
    I M P L E M E N T A T I O N
    ===========================

    CreateFrameWorkInstance
*/
SHORT _Export CreateFrameWorkInstance(
        PCHAR szFrameWorkName,
        CCFrameWork **ppFW)
{
    SHORT sRc;

    if (stricmp(szFrameWorkName, CCTAFW) == 0)
    {   /* create an instance of MainTransactionEvent */
        *ppFW = (CCFrameWork *)new MainTransactionEvent;

        sRc = 0;
    }
    else
    {
        sRc = -1;
    }

    return(sRc);
}

